<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckBracketsController extends AbstractController
{
    /**
     * @Route("/check-brackets/{expression}", name="check_brackets")
     */
    public function checkBrackets(string $expression): Response
    {
        $stack = [];
        $brackets = [
        '(' => ')',
        '[' => ']',
        '{' => '}'];
                
        foreach (str_split($expression) as $char) {
            if (isset($brackets[$char])) {
                $stack[] = $char;
            } elseif (in_array($char, $brackets)) {
                if (empty($stack) || $brackets[array_pop($stack)] !== $char) {
                    return 'Incorrect';
                }
            }
        }

        return new Response(empty($stack) ? 'Correct' : 'Incorrect');
    }
}
